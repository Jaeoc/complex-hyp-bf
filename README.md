This package provides an easy way to test hypotheses about continous predictors in multiple regression. A hypothesis may for example be that variable1 and variable2 both have a positive correlation with the outcome variable, but that the correlation of variable1 is stronger. This package enables formal testing of such hypotheses and is particularly useful for testing multiple contradicting hypotheses.


##Example use


##Installation